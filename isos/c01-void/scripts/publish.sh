#! /usr/bin/env sh
# shellcheck shell=sh
# -*- coding: utf-8 -*-
# -*- mode: shell-script -*-

#set -x

# Copyright (c) 2020 privb0x23
# OpenBSD Licence

# ==================================================================

f_libcommon='../.lib/common.sh'

# shellcheck source=../../.lib/common.sh
. "${f_libcommon}"

# ==================================================================

# shellcheck disable=SC2034
m_version='23.1.16+2020-08-29'

# ==================================================================

f_defaultconf="${d_base}/conf/void.conf"
f_localconf="${d_base}/conf/void.local.conf"

# shellcheck source=../conf/void.conf
. "${f_defaultconf}"

# shellcheck source=/dev/null
test -f "${f_localconf}" && . "${f_localconf}"

# ==================================================================

build() {

	# create images
	msg 1 'building images ...'

	ensure "${c_cp}" -f "${f_conchology}" "${d_base}/files/"

	${c_build} musl \
		&& ${c_build} glibc
}

tag() {

	# tag image
	msg 1 'updating image tag ...'
	${c_container} tag "${cont_src}${m_account}/${m_name}:musl" "${cont_src}${m_account}/${m_name}:latest"
}

push() {

	# push image
	msg 1 'pushing images to registry ...'

	# shellcheck disable=SC2086
	${c_container} push ${opts_push} "${cont_src}${m_account}/${m_name}:latest" "${cont_dst}${m_account}/${m_name}:latest" \
		&& ${c_container} push ${opts_push} "${cont_src}${m_account}/${m_name}:musl" "${cont_dst}${m_account}/${m_name}:musl" \
		&& ${c_container} push ${opts_push} "${cont_src}${m_account}/${m_name}:glibc" "${cont_dst}${m_account}/${m_name}:glibc" \
		&& ${c_container} pull "${cont_dst}${m_account}/${m_name}:latest" \
		&& ${c_container} pull "${cont_dst}${m_account}/${m_name}:musl" \
		&& ${c_container} pull "${cont_dst}${m_account}/${m_name}:glibc"
}

save() {

	${c_rm} -vf "${d_save}/${m_name}_latest.tar" "${d_save}/${m_name}_musl.tar" "${d_save}/${m_name}_glibc.tar"
	# save images
	msg 1 'saving to tar files ...'
	${c_container} save -o "${d_save}/${m_name}_latest.tar" "${cont_dst}${m_account}/${m_name}:latest" \
		&& ${c_container} save -o "${d_save}/${m_name}_musl.tar" "${cont_dst}${m_account}/${m_name}:musl" \
		&& ${c_container} save -o "${d_save}/${m_name}_glibc.tar" "${cont_dst}${m_account}/${m_name}:glibc"
}

# ==================================================================

main() {

	if test ${#} -eq 0; then

		build \
			&& tag \
			&& push \
			&& save

	else

		case "${1}" in
			build)
				shift
				build "${@}"
			;;
			tag)
				tag
			;;
			push)
				push
			;;
			save)
				save
			;;
			*)
				die 2 'invalid option'
			;;
		esac
	fi
}

main "${@}"

# ==================================================================

# vim: tabstop=4 shiftwidth=4 autoindent filetype=sh
# eof publish.sh
