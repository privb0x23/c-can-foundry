#! /usr/bin/env sh
# shellcheck shell=sh
# -*- coding: utf-8 -*-
# -*- mode: shell-script -*-

#set -x

# 0.2.15+2020-08-23

# Copyright (c) 2020 privb0x23
# OpenBSD Licence

# ==================================================================

f_libcommon='../.lib/common.sh'

# shellcheck source=../../.lib/common.sh
. "${f_libcommon}"

# ==================================================================

f_defaultconf="${d_base}/conf/void.conf"
f_localconf="${d_base}/conf/void.local.conf"
f_config="${d_base}/conf/envvars"

# shellcheck source=../conf/void.conf
. "${f_defaultconf}"

# shellcheck source=/dev/null
. "${f_localconf}"

# shellcheck source=../conf/envvars
. "${f_config}"

# ==================================================================

main() {

	mode="${1}"
	f_config_type=''

	# check mode
	# shellcheck disable=SC2015
	test 'x' = "x${mode}" \
		&& mode='musl' \
		|| shift

	if test 'xmusl' = "x${mode}"; then
		f_config_type="${d_base}/conf/void-musl.conf"

	elif test 'xglibc' = "x${mode}"; then
		f_config_type="${d_base}/conf/void-glibc.conf"

	else
		die 2 'invalid mode'
	fi

	msg
	msg 1 "building ${mode} ..."
	msg

	# generate envvars
	${c_sed} 's/\s*#.*//g;/^\s*$/d' "${f_config}" > "${d_base}/files/${envvars}"

	# generate Dockerfile
	${c_generate} "${f_config_type}" "${d_base}/templates/${f_buildfile}" > "${d_base}/${f_buildfile}"

	# generate docker-compose.yml
	#${c_generate} "${f_config_type}" "./templates/${f_composefile}" > "./${f_composefile}"

	#BUILD_DATE="$(${c_date} --utc -I)" ${c_container_compose} --file "./${f_composefile}" build "${@}"

	# shellcheck source=../conf/void.conf
	. "${f_config_type}"

	# shellcheck source=../conf/void-musl.conf
	. '/dev/null'

	# shellcheck disable=SC2086
	${c_container} build \
		-t "${m_account}/${m_name}:${txt_type}" \
		-f "${d_base}/${f_buildfile}" ${opts_build} \
		--build-arg account="${m_account}" \
		--build-arg repo="${m_repo}" \
		--build-arg co="${m_co}" \
		--build-arg build_date="$( ${c_date} -u -I )" \
		--build-arg name="${m_name}" \
		--build-arg cont_src="${cont_build_src}" \
		--build-arg d_sv="${d_sv}" \
		--build-arg d_service="${d_service}" \
		--build-arg d_entrypoint="${d_entrypoint}" \
		--build-arg type="${txt_type}" \
		--build-arg xbps_arch="${xbps_arch}" \
		--build-arg pkgcache="${d_build_cache}" \
		"${d_base}"

	unset mode
	unset f_config_type
}

# shellcheck disable=SC2154
main "${@}" || exit "${reter}"

# ==================================================================

# vim: tabstop=4 shiftwidth=4 autoindent filetype=sh
# eof build.sh
