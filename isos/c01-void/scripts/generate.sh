#! /usr/bin/env sh
# shellcheck shell=sh
# -*- coding: utf-8 -*-
# -*- mode: shell-script -*-

#set -x

# 0.1.2+2020-01-17

# ==================================================================

f_config="${1}"
f_template="${2}"

c_cat='cat'
c_sed='sed'

retok=0
reter=1

# ==================================================================

msg() {
	printf '%s\n' "${*}"
}

# ==================================================================

# shellcheck disable=SC1090
. "${f_config}"

# ==================================================================

main() {

	# shellcheck disable=SC2016
	msg "$( eval "${c_cat}<<__END__
$(${c_sed} 's/[\$`]/\\&/g;s/<%= @\([^ ]*\) %>/${\1}/g' < "${f_template}")
__END__" | ${c_sed} 's/\s*#.*//g;/^\s*$/d' )"

	exit ${retok}
}

main "${@}"

# ==================================================================

# vim: tabstop=4 shiftwidth=4 autoindent filetype=sh
# eof generate.sh
