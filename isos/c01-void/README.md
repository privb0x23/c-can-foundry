# Void

## About

A simple image based on [Void Linux](https://voidlinux.org/).

Default (latest) tag is `musl` - using musl libc.

Also available is the tag `glibc` - using glibc.
