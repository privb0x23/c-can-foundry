# 0.1.7+2020-07-18

ARG	cont_src

FROM	${cont_src}privb0x23/void:glibc

ARG	pkgcache='/var/cache/xbps'
ARG	account="null"
ARG	repo="null"
ARG	build_date="null"
ARG	name="java"

LABEL	maintainer="${account} <${account}@localhost>" \
		image="${account}/${name}" \
		url="https://hub.docker.com/r/${account}/${name}" \
		org.label-schema.build-date="${build_date}" \
		org.label-schema.name="${account}/${name}" \
		org.label-schema.vcs-url="https://gitlab.com/${account}/${repo}/blob/master/${name}" \
		org.label-schema.version="${build_date}" \
		org.label-schema.url="https://hub.docker.com/r/${account}/${name}" \
		org.opencontainers.image.created="${build_date}" \
		org.opencontainers.image.title="${account}/${name}" \
		org.opencontainers.image.source="https://gitlab.com/${account}/${repo}/blob/master/${name}" \
		org.opencontainers.image.version="${build_date}" \
		org.opencontainers.image.url="https://hub.docker.com/r/${account}/${name}"

ENV	IMAGE "${name}"

COPY	"files/entrypoint.sh" "/usr/local/bin/entrypoint.sh"
COPY	"entrypoint.d/"* "/etc/entrypoint.d/"

RUN	set -x \
	&& chmod -v 555 "/etc/entrypoint.d/"* \
	&& chmod -v 555 "/usr/local/bin/entrypoint.sh" \
	&& xbps-install -yMS xbps \
	&& xbps-install -yMSu \
	&& xbps-install -yM \
		font-misc-misc \
		dejavu-fonts-ttf \
		openjdk11 \
	&& rm -rf \
		/usr/share/doc/* \
		/usr/share/man/* \
		/usr/share/info/* \
		/var/log/*.log \
	&& { test 'x' = "x${pkgcache}" \
	|| rm -rf "${pkgcache}"/* ; }

CMD [ "/usr/local/bin/entrypoint.sh" ]

# eof Containerfile
