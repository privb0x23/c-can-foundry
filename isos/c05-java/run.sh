#! /usr/bin/env sh
# shellcheck shell=sh

#set -x

# 0.2.9+2020-03-14

# ==================================================================

c_docker='docker'
#c_podman='podman'

c_container="${c_docker}"
#c_container="${c_podman}"

m_name='java'

m_prefix='privb0x23/'
#m_prefix='docker.io/privb0x23/'

d_home="${HOME}/.containers/${m_name}"

# ==================================================================

mkdir -vp "${d_home}"

xhost | grep '^SI:localuser:user' >'/dev/null' || xhost '+SI:localuser:user'

#exec ${c_container} run -d \
exec ${c_container} run -it \
	--rm \
	--tmpfs '/tmp' \
	-v '/tmp/.X11-unix:/tmp/.X11-unix' \
	-e "DISPLAY=unix${DISPLAY}" \
	-v "${d_home}:/root" \
	"${m_prefix}${m_name}"
#	--name 'java1' \
#	"${m_prefix}${m_name}" /bin/bash

# eof run.sh
