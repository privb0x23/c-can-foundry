#! /usr/bin/env sh
# shellcheck shell=sh

#set -x

# 0.2.2+2020-10-09

# ==================================================================

f_libcommon='../.lib/common.sh'

# shellcheck source=../.lib/common.sh
. "${f_libcommon}"

# ==================================================================

# shellcheck source=./default
. "${f_defaultconf}"

# shellcheck source=/dev/null
test -f "${f_localconf}" && . "${f_localconf}"

# ==================================================================

main() {

	type='musl'
	m_tag="${type}"
	main_prod "${@}"

	# shellcheck disable=SC2034
	type='glibc'
	m_tag="${type}"
	main_prod "${@}"
}

main "${@}"

# ==================================================================

# eof publish.sh
