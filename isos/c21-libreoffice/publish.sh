#! /usr/bin/env sh
# shellcheck shell=sh

#set -x

# 0.2.15+2020-07-14

# ==================================================================

f_libcommon='../.lib/common.sh'

# shellcheck source=../.lib/common.sh
. "${f_libcommon}"

# ==================================================================

# shellcheck source=./default
. "${f_defaultconf}"

# shellcheck source=/dev/null
test -f "${f_localconf}" && . "${f_localconf}"

# ==================================================================

main() {

	main_dev "${@}"
}

main "${@}"

# ==================================================================

# eof publish.sh
