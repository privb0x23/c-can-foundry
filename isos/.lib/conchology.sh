#! /usr/bin/env sh
# shellcheck shell=sh disable=SC2034
# -*- coding: utf-8 -*-
# -*- mode: shell-script -*-

#set -x

# Copyright (c) 2020 privb0x23
# OpenBSD Licence

# ==================================================================
# Depends
#
# - basename
# - date
# - id
#
# ChangeLog
#
# - 0.1		Initial version
#
# ==================================================================

define_defaults() {

	# vars

	m_version='0.1.4+2020-10-23'

	txt_title=''
	txt_shortdesc=''
	txt_authors=''

	c_getcmdpath='command'
	c_date='date'
	c_id='id'
	c_basename='basename'
	c_dirname='dirname'

	c_awk='awk'
	c_cat='cat'
	c_chmod='chmod'
	c_chown='chown'
	c_cp='cp'
	c_curl='curl'
	c_cut='cut'
	c_dig='dig'
	c_find='find'
	c_getent='getent'
	c_git='git'
	c_grep='grep'
	c_hash='b2sum'
	c_head='head'
	c_ip='ip'
	c_jq="jq"
	c_mkdir='mkdir'
	c_mktemp='mktemp'
	c_modprobe='modprobe'
	c_mount='mount'
	c_nsenter='nsenter'
	c_od='od'
	c_rm='rm'
	c_sed='sed'
	c_shuf='shuf'
	c_sleep='sleep'
	c_sort='sort'
	c_sudo='sudo'
	c_sysctl='sysctl'
	c_tac='tac'
	c_tee='tee'
	c_umount='umount'
	c_uniq='uniq'
	c_xargs='xargs'

	c_priv="${c_sudo}"
	#c_priv=''

	c_fetch="${c_curl}"

	c_docker='docker'
	c_dockercompose='docker-compose'

	c_podman='podman'
	c_podmancompose='podman-compose'

	#c_container="${c_docker}"
	#c_container_compose="${c_dockercompose}"
	#c_imgbuild="${c_docker}"

	c_container="${c_podman}"
	#c_container_compose="${c_podmancompose}"
	c_imgbuild="${c_podman}"

	opts_fetch='--silent --fail --show-error --location'

	c_def_msg='printout'

	msg_verbose=3
	lvl1='[i]' # info
	lvl2='[*]' # verbose
	lvl3='[+]' # debug
	lvl4='[#]' # trace
	lvle='[!]' # error

	retok=0
	reter=1
}

# ==================================================================

# base

printout() {
	printf '%s\n' "${*}"
}

printerr() {
	printout "${*}" >&2
}

errmsg() {
	printerr "${lvle} ${*}"
}

die() {

	ret="${1}"
	if test 'x' = "x${ret}"; then
		ret=1
	else
		shift
	fi
	errmsg "${*}"
	exit "${ret}"
	unset ret
}

get_cmdpath() {
	type "${1}" > '/dev/null' 2>&1 && ${c_getcmdpath} -v "${1}"
}

check_uid() {

	if test "x${uid_script}" = "x${1}"; then
		return ${retok}
	else
		return ${reter}
	fi
}

check_ok() {

	test ${?} != 0 \
		&& die ${reter} "${*}"
}

ensure() {

	"${@}"
	check_ok "command failed: ${*}"
}

ignore() {
	"${@}"
}

assert_nz() {

	if test 'x' = "x${1}"; then
		return ${reter}
	else
		return ${retok}
	fi
}

check_cmd() {

	cmd="$( get_cmdpath "${1}" )"
	if test 0 -eq $? && assert_nz "${cmd}"; then
		unset cmd
	else
		unset cmd
		die 2 "'${1}' not found"
	fi
}

cmd_exists() {

	cmd="$( get_cmdpath "${1}" )"
	if test 0 -eq $? && assert_nz "${cmd}"; then
		unset cmd
		return ${retok}
	else
		unset cmd
		return ${reter}
	fi
}

get_date() {
	${c_date} -u '+%FT%T%:z'
}

# ==================================================================

# util

msg() {

	if ! assert_nz "${1}"; then

		test 1 -le "${msg_verbose}" && ${c_def_msg}

	elif test "${1}" -le "${msg_verbose}"; then

		prefix=''

		case "${1}" in
			1)
				prefix="${lvl1}"
			;;
			2)
				prefix="${lvl2}"
			;;
			3)
				prefix="${lvl3}"
			;;
			4)
				prefix="${lvl4}"
			;;
			*)
				errmsg 'invalid msg level'
				return ${reter}
			;;
		esac

		shift

		${c_def_msg} "${prefix} ${*}"

		unset prefix
	fi
}

# ==================================================================

# base

version() {

	msg 1 "Name: ${txt_script}"
	msg 1 "Version: v${m_version}"
}

super_init() {

	msg 1 "$( get_date ) ${uid_script} ${txt_script} v${m_version}"
	msg 3 "Pwd: ${d_scriptpwd} ScriptDir: ${d_script}"
	msg 2 "[${0} ${*}]"
	msg 2 'initialising ...'
}

super_onexit() {

	#msg 2 "done"
	msg 1 "$( get_date )"
}

# ==================================================================

get_abs_path() {

	path_given="${1}"

	path_dir="$( ${c_dirname} "${path_given}" )"
	path_dir="$( CDPATH='' cd -- "${path_dir}" && pwd )"

	path_file="$( ${c_basename} "${path_given}" )"

	printout "${path_dir}/${path_file}"

	unset path_given
	unset path_dir
	unset path_file
}

run_parts() {

	assert_nz "${1}" \
		&& test -d "${1}" \
		|| return 1

	d_target="${1}"; shift

	runparts="$( ${c_find} "${d_target}" -mindepth 1 -maxdepth 1 -type f -perm '/111' | ${c_sort} )"

	for i in ${runparts}; do

		test -x "${i}" && ${i} "${@}"
	done

	unset d_target
}

# 1: uri to fetch
# 2: output file name/path (blank or '-' outputs to stdout)
# 3: (optional) use metadata ('true') - default: false
fetch() {

	assert_nz "${1}" \
		|| return 1

	uri="${1}"; shift
	f_name="${1}"
	use_metadata="${2}"

	ret=0
	f_metadata=''
	download='no'
	etag=''
	http_code=''

	if assert_nz "${f_name}"; then
		shift

		if test 'x--' = "x${f_name}"; then
			f_name='-'

		elif assert_nz "${use_metadata}"; then
			shift

			if test 'x--' = "x${1}"; then
				shift
			fi

			# when using metadata, check for a filename (not empty or '-')
			if test 'xtrue' = "x${use_metadata}" \
				&& test 'x-' != "x${f_name}"; then

				f_metadata="${f_name}.meta"
			fi
		fi

	else
		f_name='-'
	fi

	if assert_nz "${f_metadata}" && test -f "${f_metadata}"; then

		# extract the etag string from the existing metadata
		# shellcheck disable=SC2016
		etag="$( ${c_tac} "${f_metadata}" \
			| ${c_awk} -F '"' 'BEGIN{IGNORECASE = 1}/^etag/{print $2}' \
			| ${c_head} -n 1 )" \
			|| ret=$?

		if test ${ret} -eq 0 && assert_nz "${etag}"; then

			#msg 3 "checking ${uri} ..."

			# shellcheck disable=SC2086
			set -- ${opts_fetch} "${@}"
			http_code="$( ${c_fetch} "${@}" \
				--url "${uri}" \
				--head \
				--header "If-None-Match: \"${etag}\"" \
				--output '/dev/null' \
				--write-out '%{http_code}' )" \
				|| ret=$?

			if test 'x200' = "x${http_code}"; then
				download='yes'
			elif test 'x304' != "x${http_code}"; then
				errmsg "Non 200 response for '${uri}': ${http_code}"
			fi

		else
			download='yes'
		fi

	else
		download='yes'
	fi

	if test ${ret} -eq 0 && test 'xyes' = "x${download}"; then

		#msg 3 "fetching '${uri}' ..."

		if assert_nz "${f_metadata}"; then

			# download the file and create the metadata
			# shellcheck disable=SC2086
			set -- ${opts_fetch} "${@}"
			http_code="$( ${c_fetch} "${@}" \
				--url "${uri}" \
				--output "${f_name}" \
				--dump-header "${f_metadata}" \
				--write-out '%{http_code}' )" \
				|| ret=$?

			if test 'x200' != "x${http_code}"; then
				errmsg "Non 200 response for '${uri}': ${http_code}"
			fi

		else

			# download the file
			# shellcheck disable=SC2086
			set -- ${opts_fetch} "${@}"
			${c_fetch} "${@}" \
				--url "${uri}" \
				--output "${f_name}" \
				|| ret=$?

		fi
	fi

	unset uri
	unset f_name
	unset use_metadata
	unset f_metadata
	unset download
	unset etag
	unset http_code

	return ${ret}
	unset ret
}

resolve() {

	assert_nz "${1}" \
		|| return 1

	ret=0
	h_target="${1}"

	if cmd_exists "${c_dig}"; then

		# shellcheck disable=SC2016
		${c_dig} +timeout=10 +retry=3 +noall +answer "${h_target}" A \
			| ${c_awk} '$4=="A"{print $5}' | ${c_sort} | ${c_uniq}

		# shellcheck disable=SC2016
		${c_dig} +timeout=10 +retry=3 +noall +answer "${h_target}" AAAA \
			| ${c_awk} '$4=="AAAA"{print $5}' | ${c_sort} | ${c_uniq}

	elif ${c_getent} ahosts >'/dev/null' 2>&1; then

		# shellcheck disable=SC2016
		${c_getent} ahosts "${h_target}" \
			| ${c_awk} '{print $1}' | ${c_sort} | ${c_uniq} \
			|| ret=$?

	else
		# shellcheck disable=SC2016
		${c_getent} hosts "${h_target}" \
			| ${c_awk} '{print $1}' | ${c_sort} | ${c_uniq} \
			|| ret=$?
	fi

	unset h_target

	return ${ret}
	unset ret
}

# ==================================================================

auto_run() {

	define_defaults

	check_cmd "${c_basename}"
	#check_cmd "${c_dirname}"
	check_cmd "${c_date}"
	check_cmd "${c_id}"

	uid_script="$( ${c_id} -u )"

	txt_script="$( ${c_basename} "${0}" )"
	d_scriptpwd="$( pwd -P )"

	if ! assert_nz "${d_script}"; then
		d_script="/${0}"; d_script="${d_script%/*}"; d_script="${d_script:-.}"; d_script="${d_script#/}/"
		d_script="$( CDPATH='' cd -- "${d_script}" && pwd -P )"
	fi
}

auto_run

# ==================================================================

# vim: tabstop=4 shiftwidth=4 autoindent filetype=sh
# eof conchology.sh
