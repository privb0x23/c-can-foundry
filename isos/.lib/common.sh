#! /usr/bin/env sh
# shellcheck shell=sh
# -*- coding: utf-8 -*-
# -*- mode: shell-script -*-

#set -x

# Copyright (c) 2020 privb0x23
# OpenBSD Licence

# ==================================================================

f_conchology="../.lib/conchology.sh"
# shellcheck source=./conchology.sh disable=SC2015
test -f "${f_conchology}" && . "${f_conchology}" || exit 1

# ==================================================================

# shellcheck disable=SC2034
m_version='0.1.5+2020-10-09'

# ==================================================================

test 'x' = "x${f_globaldefconf}" \
	&& f_globaldefconf='../.conf/default'

# shellcheck source=../.conf/default
. "${f_globaldefconf}"

d_save="${HOME}"

# ==================================================================

build() {

	# create images
	msg 1 'building image ...'

	ensure "${c_cp}" -f "${f_conchology}" "${d_base}/files/"

	#BUILD_DATE="$( ${c_date} -u -I )" ${c_container_compose} --file "./${f_composefile}" build "${@}"

	# shellcheck disable=SC2086
	${c_container} build \
		-t "${m_account}/${m_name}:${m_tag}" \
		-f "${f_buildfile}" ${opts_build} \
		--build-arg account="${m_account}" \
		--build-arg repo="${m_repo}" \
		--build-arg co="${m_co}" \
		--build-arg build_date="$( ${c_date} -u -I )" \
		--build-arg name="${m_name}" \
		--build-arg cont_src="${cont_build_src}" \
		--build-arg pkgcache="${d_build_cache}" \
		--build-arg d_sv="${d_sv}" \
		--build-arg d_service="${d_service}" \
		--build-arg d_entrypoint="${d_entrypoint}" \
		--build-arg type="${type}" \
		"${@}" \
		"${d_base}"
}

tag() {

	# tag image
	msg 1 'updating image tag ...'
	${c_container} tag "${cont_src}${m_account}/${m_name}:${m_tag}" "${cont_src}${m_account}/${m_name}:latest"
}

push() {

	# push image
	msg 1 'pushing image to registry ...'
	# shellcheck disable=SC2086
	${c_container} push ${opts_push} "${cont_src}${m_account}/${m_name}:${m_tag}" "${cont_dst}${m_account}/${m_name}:${m_tag}" \
		&& ${c_container} pull "${cont_dst}${m_account}/${m_name}:${m_tag}"
}

save() {

	${c_rm} -vf "${d_save}/${m_name}_${m_tag}.tar"
	# save image
	msg 1 'saving to tar file ...'
	${c_container} save -o "${d_save}/${m_name}_${m_tag}.tar" "${cont_dst}${m_account}/${m_name}:${m_tag}"
}

# ==================================================================

main_prod() {

	if test ${#} -eq 0; then

		build \
			&& push \
			&& save
			#&& tag \

	else

		case "${1}" in
			build)
				shift
				build "${@}"
			;;
			#tag)
			#	tag
			#;;
			push)
				push
			;;
			save)
				save
			;;
			*)
				die 2 "invalid option"
			;;
		esac
	fi
}

main_dev() {

	if test ${#} -eq 0; then

		build \
			&& save
			#&& tag \
			#&& push \

	else

		case "${1}" in
			build)
				shift
				build "${@}"
			;;
			#tag)
			#	tag
			#;;
			#push)
			#	push
			#;;
			save)
				save
			;;
			*)
				die 2 "invalid option"
			;;
		esac
	fi
}

# ==================================================================

# vim: tabstop=4 shiftwidth=4 autoindent filetype=sh
# eof common.sh
