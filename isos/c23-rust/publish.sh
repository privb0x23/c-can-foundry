#! /usr/bin/env sh
# shellcheck shell=sh

#set -x

# 0.2.15+2020-10-10

# ==================================================================

f_libcommon='../.lib/common.sh'

# shellcheck source=../.lib/common.sh
. "${f_libcommon}"

# ==================================================================

# shellcheck source=./default
. "${f_defaultconf}"

# shellcheck source=/dev/null
test -f "${f_localconf}" && . "${f_localconf}"

# ==================================================================

main() {

	type='musl'
	m_tag="${type}"
	main_dev "${@}"

	type='glibc'
	m_tag="${type}"
	main_dev "${@}"
}

main "${@}"

# ==================================================================

# eof publish.sh
