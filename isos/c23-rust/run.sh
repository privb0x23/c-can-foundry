#! /usr/bin/env sh
# shellcheck shell=sh

#set -x

# 0.2.7+2020-02-01

# ==================================================================

c_docker='docker'
#c_podman='podman'

c_container="${c_docker}"
#c_container="${c_podman}"

m_name='rust'

m_prefix='privb0x23/'
#m_prefix='docker.io/privb0x23/'

d_home="${HOME}/.containers/${m_name}"

# ==================================================================

mkdir -vp "${d_home}"

#exec ${c_container} run -d \
exec ${c_container} run -it \
	--rm --tmpfs '/tmp' \
	-v "${d_home}:/root" \
	"${m_prefix}${m_name}" /bin/bash
#	"${m_prefix}${m_name}"
#	--name 'rust1' \

# eof run.sh
