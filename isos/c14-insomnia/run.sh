#! /usr/bin/env sh
# shellcheck shell=sh

#set -x

# 0.2.5+2020-03-14

# ==================================================================

c_docker='docker'
#c_podman='podman'

c_container="${c_docker}"
#c_container="${c_podman}"

m_name='insomnia'

m_prefix='privb0x23/'
#m_prefix='docker.io/privb0x23/'

d_home="${HOME}/.containers/${m_name}"

# ==================================================================

mkdir -vp "${d_home}"

xhost | grep '^SI:localuser:user' >'/dev/null' || xhost '+SI:localuser:user'

XAUTH="$( xauth list | awk "/:$( printf '%s\n' "${DISPLAY}" | awk -F ':' '{print $2}' | awk -F '.' '{print $1}' )/{print $3}" | head -n 1 )"

#exec ${c_container} run -it \
exec ${c_container} run -d \
	--rm \
	--env "DISPLAY=unix${DISPLAY}" \
	--env "XAUTH=${XAUTH}" \
	--volume '/tmp/.X11-unix:/tmp/.X11-unix' \
	--volume "${d_home}:/home/user" \
	"${m_prefix}${m_name}"
#	--name 'insomnia1' \
#	"${m_prefix}${m_name}" /bin/bash

# eof run.sh
