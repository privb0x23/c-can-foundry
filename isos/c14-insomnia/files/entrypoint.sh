#! /usr/bin/env sh
# shellcheck shell=sh

# 0.1.4+2020-09-09

c_rm='rm'
c_xauth='xauth'

test 'x' != "x${XAUTH}" \
	&& ${c_rm} -f "${HOME}/.Xauthority" \
	&& ${c_xauth} add "${DISPLAY}" . "${XAUTH}"

exec /usr/bin/insomnia

# eof entrypoint.sh
