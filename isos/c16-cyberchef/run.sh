#! /usr/bin/env sh
# shellcheck shell=sh

#set -x

# 0.2.1+2020-04-08

# ==================================================================

c_docker='docker'
#c_podman='podman'

c_container="${c_docker}"
#c_container="${c_podman}"

m_name='cyberchef'

#m_prefix='privb0x23/'
m_prefix='docker.io/privb0x23/'

ip_httpsrv=''

# ==================================================================

sudo ${c_container} network create --internal 'cyberchef'

#exec sudo ${c_container} run -it \
exec sudo ${c_container} run -d \
	--rm \
	--tmpfs '/tmp' \
	--network 'cyberchef' \
	-p "${ip_httpsrv}:8080:8080" \
	"${m_prefix}${m_name}"
#	"${m_prefix}${m_name}" /bin/sh

# eof run.sh
