#! /usr/bin/env sh
# shellcheck shell=sh

# 0.1.2+2020-07-23

# ==================================================================

if test ${#} -eq 0; then

	exec /bin/bash

else

	test 'x--' = "x${1}" && shift

	exec "${@}"
fi

# eof entrypoint.sh
