#! /usr/bin/env sh
# shellcheck shell=sh

# 0.1.10+2020-05-27

c_vscodium='/usr/local/bin/codium'

exec "${c_vscodium}" --no-sandbox --user-data-dir "${DATA_DIR}"

# eof entrypoint.sh
