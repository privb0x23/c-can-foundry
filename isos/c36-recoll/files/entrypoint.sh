#! /usr/bin/env sh
# shellcheck shell=sh

# 0.1.1+2020-07-22

mode='query'

while true; do
	case "${1}" in

		index)
			mode='index'
			shift
		;;

		query)
			mode='query'
			shift
		;;

		--)
			shift
			break
		;;

		*)
			break
		;;
	esac
done

case "${mode}" in

	index)
		exec /usr/bin/recollindex -c "/mnt/shared/.recoll" "${@}"
	;;

	query)
		exec /usr/bin/recollq -c "/mnt/shared/.recoll" "${@}"
	;;

	*)
		exit 1
	;;
esac

# eof entrypoint.sh
