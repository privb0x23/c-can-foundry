#! /usr/bin/env sh
# shellcheck shell=sh

#set -x

# 0.2.1+2020-07-23

# ==================================================================

c_docker='docker'
#c_podman='podman'

c_container="${c_docker}"
#c_container="${c_podman}"

m_name='recoll'

m_prefix='privb0x23/'
#m_prefix='docker.io/privb0x23/'

d_home="${HOME}/.containers/${m_name}"

d_recoll='./.recoll'

# ==================================================================

mkdir -vp "${d_home}"

#exec ${c_container} run -it \
exec ${c_container} run \
	--rm \
	--network 'none' \
	--tmpfs '/tmp' \
	--volume "${d_home}:/home/user" \
	--volume "./:/mnt/shared" \
	--volume "${d_recoll}:/mnt/shared/.recoll" \
	"${m_prefix}${m_name}" \
	"${@}"
#	"${m_prefix}${m_name}" /bin/bash

# eof run.sh
