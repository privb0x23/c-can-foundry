#! /usr/bin/env sh
# shellcheck shell=sh

#set -x

# 0.2.1+2020-03-23

# ==================================================================

c_docker='docker'
#c_podman='podman'

c_container="${c_docker}"
#c_container="${c_podman}"

m_name='ghidra'

#m_prefix='privb0x23/'
m_prefix='docker.io/privb0x23/'

d_home="${HOME}/.containers/${m_name}"

# ==================================================================

mkdir -vp "${d_home}"

sudo ${c_container} network create --internal 'ghidra'

#exec sudo ${c_container} run -it \
exec sudo ${c_container} run -d \
	--rm \
	--network 'ghidra' \
	--tmpfs '/var/log' \
	--tmpfs '/dev/shm' \
	--volume "${d_home}:/home/user" \
	--name 'ghidra-server' \
	"${m_prefix}${m_name}" server
#	"${m_prefix}${m_name}" /bin/bash

# eof server.sh
