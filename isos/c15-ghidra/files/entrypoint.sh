#! /usr/bin/env sh
# shellcheck shell=sh

# 0.1.0+2020-03-23

d_ghidra='/usr/libexec/ghidra'

if test 'xclient' = "x${1}"; then

	shift

	MAXMEM=${MAXMEM:-768M}

	# client
	exec "${d_ghidra}"/support/launch.sh fg Ghidra "${MAXMEM}" "" ghidra.GhidraRun "${@}"

elif test 'xserver' = "x${1}"; then

	# server
	exec "${d_ghidra}"/server/ghidraSvr console

else

	exit 1
fi

# eof entrypoint.sh
