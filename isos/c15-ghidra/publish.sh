#! /usr/bin/env sh
# shellcheck shell=sh

#set -x

# 0.2.3+2020-07-16

# ==================================================================

f_libcommon='../.lib/common.sh'

# shellcheck source=../.lib/common.sh
. "${f_libcommon}"

# ==================================================================

# shellcheck source=./default
. "${f_defaultconf}"

# shellcheck source=/dev/null
test -f "${f_localconf}" && . "${f_localconf}"

# ==================================================================

main() {

	main_dev "${@}"
}

main "${@}"

# ==================================================================

# eof publish.sh
