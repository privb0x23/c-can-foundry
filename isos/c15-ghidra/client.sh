#! /usr/bin/env sh
# shellcheck shell=sh

#set -x

# 0.2.1+2020-03-23

# ==================================================================

c_docker='docker'
#c_podman='podman'

c_container="${c_docker}"
#c_container="${c_podman}"

m_name='ghidra'

m_prefix='privb0x23/'
#m_prefix='docker.io/privb0x23/'

d_home="${HOME}/.containers/${m_name}"

# ==================================================================

mkdir -vp "${d_home}"

xhost | grep '^SI:localuser:user' >'/dev/null' || xhost '+SI:localuser:user'

XAUTH="$( xauth list | awk "/:$( printf '%s\n' "${DISPLAY}" | awk -F ':' '{print $2}' | awk -F '.' '{print $1}' )/{print $3}" | head -n 1 )"

#exec sudo ${c_container} run -it \
exec sudo ${c_container} run -d \
	--rm \
	--network 'container:ghidra-server' \
	--tmpfs '/tmp' \
	--tmpfs '/var/log' \
	--tmpfs '/dev/shm' \
	--env "DISPLAY=unix${DISPLAY}" \
	--env "XAUTH=${XAUTH}" \
	--volume '/tmp/.X11-unix:/tmp/.X11-unix' \
	--volume "${d_home}:/home/user" \
	--name 'ghidra-client' \
	"${m_prefix}${m_name}" client
#	"${m_prefix}${m_name}" /bin/bash

# eof client.sh
