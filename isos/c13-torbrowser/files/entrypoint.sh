#! /usr/bin/env sh
# shellcheck shell=sh

# 0.2.5+2020-06-29

username='user'
d_home="/home/${username}"

d_tb="${d_home}/.tb/tor-browser"

c_mkdir='mkdir'
c_secbrowser='/usr/local/bin/torbrowser'
c_sed='sed'
c_tar='tar'
c_tb="${d_tb}/Browser/start-tor-browser"

f_tb_pkg='/var/tmp/tb.tar.xz'

# extract fresh install
if ! test -x "${c_tb}"; then
	${c_mkdir} -p "${d_tb}"
	printf '%s\n' 'extracting fresh tor browser'
	${c_tar} -Jx --strip-components 1 -C "${d_tb}" -f "${f_tb_pkg}"
	${c_sed} -i'' '96s/^\s/#\t/' "${c_tb}"
fi

if test 'x' = "x${SECBROWSER}"; then

	# run tor-browser
	exec ${c_tb} --log '/dev/stdout'

else

	# shellcheck source=../secbrowser/variables.bsh
	. /usr/share/secbrowser/variables.bsh

	export tb_home_folder="${d_home}/.tb"
	export tb_browser_folder="${d_tb}"

	# run secbrowser
	exec ${c_secbrowser} --log '/dev/stdout'

fi

# eof entrypoint.sh
