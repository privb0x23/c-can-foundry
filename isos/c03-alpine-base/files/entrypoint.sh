#! /usr/bin/env sh
# shellcheck shell=sh

# 0.3.9+2020-04-07

# ==================================================================

d_entrypoint=''
d_service=''

c_runparts='/bin/run-parts'
c_runsvdir='/sbin/runsvdir'

d_env='/etc/container_environment'
envvars='agent'
f_defconf="${d_env}/${envvars}"
f_localconf="/tmp/${envvars}"

# ==================================================================

env > "${f_localconf}"

# shellcheck disable=SC1090
. "${f_defconf}"
# shellcheck disable=SC1090
. "${f_localconf}"

# ==================================================================

main() {

	printf '%s\n' "Starting pre-service scripts in ${d_entrypoint}"

	if test -d "${d_entrypoint}"; then

		${c_runparts} "${d_entrypoint}" > '/dev/null'
	fi

	if test ${#} -eq 0; then

		exec ${c_runsvdir} -P "${d_service}"

	else

		${c_runsvdir} -P "${d_service}" &

		test "${1}" = '--' && shift

		exec "${@}"
	fi
}

main "${@}"

# ==================================================================

# eof entrypoint.sh
