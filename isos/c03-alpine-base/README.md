# Alpine-Base

## About

A simple image based on [Alpine Linux](https://alpinelinux.org/).

Only the default (latest) tag is available.
